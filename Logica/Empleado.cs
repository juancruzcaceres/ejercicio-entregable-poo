﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Empleado
    {
        public int DNI { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public DateTime FechaIngreso { get; set; }
        public decimal SueldoBruto { get; set; }
        public string Seccion { get; set; }

        public int ObtenerAntiguedad()
        {
            return (DateTime.Today.Year - FechaIngreso.Year);
        }

        public decimal ObtenerSueldoNeto()
        {
            switch (Seccion)
            {
                case "A":
                    return SueldoBruto *80/100;
                case "P":
                    return SueldoBruto-ObtenerAntiguedad()*2/100;
                case "V":
                    if (ObtenerAntiguedad()>5)
                    {
                        return SueldoBruto - ObtenerAntiguedad() * 3 / 100;
                    }
                    return SueldoBruto;
                default:
                    return 0;
            }
        }
    }
}
