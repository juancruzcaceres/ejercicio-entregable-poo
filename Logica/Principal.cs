﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        public List<Empleado> listaEmpleados { get; set; }
        public List<EmpleadoDeSeccion> listaEmpleadosDeSeccion { get; set; }

        public Empleado ObtenerEmpleado(int dni)
        {
            foreach (var empleado in listaEmpleados)
            {
                if (empleado.DNI==dni)
                {
                    return empleado;
                }
            }
            return null;
        }

        public void RegistrarEmpleado(int dni, string nombre, DateTime fechaNacimiento, DateTime fechaIngreso, decimal sueldoBruto)
        {
            if (ObtenerEmpleado(dni)==null)
            {
                Empleado nuevoEmpleado = new Empleado();
                nuevoEmpleado.DNI = dni;
                nuevoEmpleado.Nombre = nombre;
                nuevoEmpleado.FechaNacimiento = fechaNacimiento;
                nuevoEmpleado.FechaIngreso = fechaIngreso;
                nuevoEmpleado.SueldoBruto = sueldoBruto;
                listaEmpleados.Add(nuevoEmpleado);
            }
        }

        public decimal DesvincularEmpleado(int dni)
        {
            decimal indemnizacion = 2 * ObtenerEmpleado(dni).SueldoBruto * ObtenerEmpleado(dni).ObtenerAntiguedad();
            listaEmpleados.Remove(ObtenerEmpleado(dni));
            return indemnizacion;
        }

        public List<EmpleadoDeSeccion> ObtenerListadoEmpleadosDeSeccion(string seccion)
        {
            foreach (var empleado in listaEmpleados)
            {
                if (empleado.Seccion==seccion)
                {
                    EmpleadoDeSeccion empleadoDeSeccion = new EmpleadoDeSeccion();
                    empleadoDeSeccion.Antiguedad = empleado.ObtenerAntiguedad();
                    empleadoDeSeccion.Nombre = empleado.Nombre;
                    empleadoDeSeccion.Edad = empleado.FechaNacimiento.Year - DateTime.Today.Year;
                    empleadoDeSeccion.Seccion = empleado.Seccion;
                    listaEmpleadosDeSeccion.Add(empleadoDeSeccion);
                }
            }
            listaEmpleadosDeSeccion.OrderByDescending(x => x.Antiguedad);
            return listaEmpleadosDeSeccion;
        }
    }
}
